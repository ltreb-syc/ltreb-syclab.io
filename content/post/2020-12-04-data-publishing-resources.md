---
title: data-publishing resources
subtitle: resources for publishing Sycamore Creek data
date: 2020-12-18
tags: ["data-publishing"]
---

+ dataset: [knb-lter-cap.375](https://gitlab.com/ltreb-syc/knb-lter-cap.375)
  - title: Macroinvertebrate collections following floods in Sycamore Creek, Arizona, USA (1985-1999)
  - short-name: macroinvertebrates and flooding

---

+ dataset: [knb-lter-cap.589](https://gitlab.com/ltreb-syc/knb-lter-cap.589)
  - title: Long-term monitoring of macroinvertebrates in Sycamore Creek, Arizona, USA (2010-2019)
  - short-name: macroinvertebrates

---

+ dataset: [knb-lter-cap.590](https://gitlab.com/ltreb-syc/knb-lter-cap.590)
  - title: Algal Nutrient Limitation Bioassays in Sycamore Creek, Arizona, USA (2010-2020)
  - short-name: NDS

---

+ dataset: [knb-lter-cap.593](https://gitlab.com/ltreb-syc/knb-lter-cap.593)
  - title: Long-term measurements of algal biomass in Sycamore Creek, Arizona, USA (2011-2019)
  - short-name: algae

---

+ dataset: [knb-lter-cap.594](https://gitlab.com/ltreb-syc/knb-lter-cap.594)
  - title: Long-term record of wetted area, substrate type, and plant features in Sycamore Creek, Arizona, USA (2010-2020)
  - short-name: transects

---

+ dataset: [knb-lter-cap.596](https://gitlab.com/ltreb-syc/knb-lter-cap.596)
  - title: Wetland abundance and distribution changes in Sycamore Creek, Arizona, USA (2014-2019)
  - short-name: 12K

---

+ dataset: [knb-lter-cap.606](https://gitlab.com/ltreb-syc/knb-lter-cap.606)
  - title: Long-term monitoring of streamwater chemistry in Sycamore Creek, Arizona, USA (2010-2014)
  - short-name: chemistry

---

+ dataset: [knb-lter-cap.689](https://gitlab.com/ltreb-syc/knb-lter-cap.689)
  - title: Long-term monitoring of floodwater chemistry in Sycamore Creek, Arizona, USA (2010-2020)
  - short-name: floodwater chemistry
