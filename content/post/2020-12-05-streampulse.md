---
title: streamPULSE
subtitle: streamPULSE cross-site study
date: 2020-12-05
tags: ["metabolism", "biogeochemistry"]
---

StreamPULSE is a community of researchers building the scientific capacity to answer these types of questions with modern sensor technology and novel modeling infrastructure. [data](https://streampulse.org/products.html)
