---
title: NDS
subtitle: Algal Nutrient Limitation Bioassays in Sycamore Creek, Arizona, USA (2010-2020)
date: 2020-12-06
tags: ["algae", "biogeochemistry"]
bigimg: [{src: "/img/random-flood.jpg", desc: "random flooding"}]
---

*title*

Algal Nutrient Limitation Bioassays in Sycamore Creek, Arizona, USA (2010-2020)

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This dataset addresses patterns of nutrient limitation by primary producers as indicated by *in situ* growth on artificial substrates.

*data*

[knb-lter-cap.590](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=590)
