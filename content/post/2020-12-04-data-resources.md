---
title: data resources
subtitle: Sycamore Creek data resources
date: 2020-12-04
tags: ["data"]
---


**wetland abundance and distribution (12K):**

- Grimm, N., J. Sabo, S. Bonjour, X. Dong, A. Handler, K. Kemmitt, M. Lauck, M. Palta, and L. Pollard. 2020. Wetland abundance and distribution changes in Sycamore Creek, Arizona, USA (2014-2019) ver 2. Environmental Data Initiative. [data](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=596)
