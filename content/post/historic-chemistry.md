---
title: historic-chemistry
subtitle: Long-term record of streamwater chemistry in Sycamore Creek, Arizona, USA (1977-1999)
date: 2021-07-07
tags: ["biogeochemistry"]
bigimg: [{src: "/img/random-labglass.jpg", desc: "random glassware"}]
---

*title*

Long-term record of streamwater chemistry in Sycamore Creek, Arizona, USA (1977-1999)

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This specific dataset was collected to monitor long-term changes in dissolved nutrient concentrations (e.g., nitrogen, phosphorus) and other water-quality parameters by sampling surface water.

*data*

[knb-lter-cap.392](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=392)
