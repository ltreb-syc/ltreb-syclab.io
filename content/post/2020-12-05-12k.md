---
title: 12K
subtitle: Wetland abundance and distribution changes in Sycamore Creek, Arizona, USA (2014-2019)
date: 2020-12-05
tags: ["wetland", "plants", "algae"]
bigimg: [{src: "/img/random-wetland.jpg", desc: "random wetland"}]
---

*title*

Wetland abundance and distribution changes in Sycamore Creek, Arizona, USA (2014-2019)

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This dataset was collected to understand: (1) the spatial pattern of wetland distribution and abundance; (2) the influence of multi-annual variability in hydrological regime on wetland distribution and abundance; and (3) the mechanism of the resilience of wetland to different disturbances in terms of hydrology (i.e., drying and flooding).

*data*

[knb-lter-cap.596](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=596)
