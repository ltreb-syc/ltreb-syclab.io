---
title: dissolved organic carbon
subtitle: Dissolved organic carbon dynamics in an urban desert stream ecosystem in central Arizona-Phoenix
date: 2020-12-06
tags: ["biogeochemistry"]
bigimg: [{src: "/img/random-flood.jpg", desc: "random flooding"}]
---

*title*

Dissolved organic carbon dynamics in an urban desert stream ecosystem in central Arizona-Phoenix

*abstract*

Variation in stream chemistry is a function of the strength of terrestrial-aquatic linkages, the extent to which surface and groundwater exchanges, and the rate of instream biotic processes. The importance of these variables may fluctuate as a function of climate regime, catchment geomorphology, or level of human impact. A mechanistic understanding of the influence of each of these variables on ecosystem functioning will increase understanding of the role of streams in global nutrient and carbon cycles. Of particular interest is dissolved organic carbon (DOC), an important source of carbon and energy for microbial processes. Respiration by heterotrophic bacterial communities has recently been linked to the quality (ability of microbes to utilize C source) of the DOC pool in streams. DOC not only influences stream nutrient supply, but also the transport of contaminants and the attenuation of UV radiation. This dissertation focused on DOC delivery to two arid-land stream ecosystems, one native desert (Sycamore Creek, AZ), and one urban (Phoenix, AZ). The overall objectives of this work were to (1) document patterns in DOC quantity and chemical composition in response to flooding and groundwater exchange, (2) generate and test hypotheses explaining variation in DOC quantity and quality and (3) relate this variation to microbial activity. In the native desert stream ecosystem, the climate regime influenced seasonal variation in the quantity and quality of DOC inputs, with higher complexity and higher concentrations of DOC in summer monsoonal runoff. In contrast, human alteration of geomorphology and hydrologic flowpaths in the Phoenix metropolitan area significantly influenced streamwater chemistry in comparison to low-impact streams in the Sonoran Desert. In the city, mechanisms of nutrient retention and transformation were often shifted from dominance by biotic to abiotic ones, severely dampening the influence of climate regime and substituting instead the maintenance of food production and waste management as the dominant large-scale controlling factors. Patterns of microbial respiration and extracellular enzyme production indicate that the community's ability to react to changes in DOM composition may create shifts in extracellular enzyme production while maintaining relatively consistent levels of community respiration.

*data*

- [knb-lter-cap.258](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=258)
- [knb-lter-cap.259](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=259)

