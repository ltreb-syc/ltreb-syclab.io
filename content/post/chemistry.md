---
title: chemistry
subtitle: Long-term monitoring of streamwater chemistry in Sycamore Creek, Arizona, USA (2010-2014)
date: 2020-12-18
tags: ["biogeochemistry"]
bigimg: [{src: "/img/random-labglass.jpg", desc: "random glassware"}]
---

*title*

Long-term monitoring of streamwater chemistry in Sycamore Creek, Arizona, USA (2010-2014)

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This specific dataset was collected to monitor long-term changes in dissolved nutrient concentrations (N, P, C) by sampling surface water within gravel and wetland dominated reaches during baseflow.

*data*

[knb-lter-cap.606](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=606)
