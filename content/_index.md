### project overview

This site provides background information and links to resources associated with ecological studies of Sycamore Creek in the Sonoran desert of central Arizona. The ecology of Sycamore Creek has been a central focus of the S. Fisher and [N. Grimm](https://sols.asu.edu/nancy-grimm) Labs at Arizona State University for more than four decades.

![desert](/img/random-desert.jpg)
