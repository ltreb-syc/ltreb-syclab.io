---
title: literature
subtitle: Sycamore Creek literature resources
comment: false
---

- Dong, X., A. Ruhí, and N. B. Grimm.. "Evidence for self-organization in determining spatial patterns of stream nutrients, despite primacy of the geomorphic template," Proceedings of the National Academy of Sciences of the United States, v.114, 2017, p. 4744. [doi:10.1073/pnas.1617571114](http://dx.doi.org/10.1073/pnas.1617571114)

- Sponseller, RA; Grimm, NB; Boulton, AJ; Sabo, JL. "Responses of macroinvertebrate communities to long-term flow variability in a Sonoran Desert stream," GLOBAL CHANGE BIOLOGY, v.16, 2010, p. 2891. View record at Web of Science [doi:10.1111/j.1365-2486.2010.02200](https://doi.org/10.1111/j.1365-2486.2010.02200.x)

