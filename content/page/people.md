---
title: people
subtitle: contributors to research at Sycamore Creek
comment: false
---

| contributor     | institution                                     | ORCiD                                                        |
|-----------------|-------------------------------------------------|--------------------------------------------------------------|
| Bonjour, Sophia | Kansas State University                         | [0000-0003-3614-7023](https://orcid.org/0000-0003-3614-7023) |
| Dong, Xiaoli    | University of California, Davis                 | [0000-0003-3303-0735](https://orcid.org/0000-0003-3303-0735) |
| Handler, Amalia | USEPA National Health and Environmental Effects | [0000-0001-8372-6488](https://orcid.org/0000-0001-8372-6488) |
| Lauck, Marina   | Arizona State University                        | [0000-0002-9032-0782](https://orcid.org/0000-0002-9032-0782) |
